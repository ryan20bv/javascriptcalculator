
let enteredString = "";
let valueInShowMe = "";
let fromString = "";
let operatorValue = "";
let firstNumber = 0;
let total = 0 ;


// start number button //
function zero(){
    let zeroValue = BTN_0.value;
    enteredValueHandler(zeroValue);
}
function one(){
    let oneValue = BTN_1.value;
    enteredValueHandler(oneValue);
}
function two(){
    let twoValue = BTN_2.value;
    enteredValueHandler(twoValue);
}
function three(){
    let threeValue = BTN_3.value;
    enteredValueHandler(threeValue);
}
function four(){
    let fourValue = BTN_4.value;
    enteredValueHandler(fourValue);
}
function five(){
    let fiveValue = BTN_5.value;
    enteredValueHandler(fiveValue);
}
function six(){
    let sixValue = BTN_6.value;
    enteredValueHandler(sixValue);
}
function seven(){
    let sevenValue = BTN_7.value;
    enteredValueHandler(sevenValue);
}
function eight(){
    let eightValue = BTN_8.value;
    enteredValueHandler(eightValue);
}
function nine(){
    let nineValue = BTN_9.value;
    enteredValueHandler(nineValue);
}
function pos(){
    let posValue = BTN_POS.value;
    enteredValueHandler(posValue);
}
function dot(){
    let dotValue = BTN_DOT.value;
    enteredValueHandler(dotValue);
}
// end number button //

function enteredValueHandler (enteredValue){
    enteredString += enteredValue;
    fromString += enteredValue;
    stringToNum(fromString);
    showMeValue();
}

function stringToNum(string){
    firstNumber = parseFloat(string);
    console.log("first number: ",firstNumber);
    return firstNumber;
    
}

// start operator button //
function additionHandler(){
    let addValue = ADD_BTN.value;
    checkOperatorValue(addValue);
}
function subtractionHandler(){
    let minusValue = MINUS_BTN.value;
    checkOperatorValue(minusValue);
}
function multiplicationHandler(){
    let multiValue = MUL_BTN.value;
    checkOperatorValue(multiValue);
}
function divisionHandler(){
    let divValue = DIV_BTN.value;
    checkOperatorValue(divValue);
}



function checkOperatorValue (mathValue){
    console.log(firstNumber);
    let sign
    if (!operatorValue){
        operatorValue = mathValue;
    } else {
        // deleteHandler();
        operatorValue = mathValue;
        
    }
    sign = operatorValue;
    calculate(operatorValue);
    showMeValue();
    console.log("sign: ", sign)
    fromString = "";
    operatorValue = "";
}

function calculate (operator){
    if (operator === "+"){
        total += firstNumber;
    } else if (operator === "-"){
        total -= firstNumber;
    } else if (operator === "*"){
        total *= firstNumber;
    } else if (operator === "/"){
        total /= firstNumber;
    }
    displayTotal(total);
    console.log("total: ", total)
}

function displayTotal(total){
    DISPLAY.innerText = total;
}

function deleteHandler(){
    let newString = enteredString.slice(0, -1);
    enteredString = newString;
    showMeValue();
}

function showMeValue(){
    let showValue;
    if (!operatorValue){
        showValue = enteredString;
    } else {
        showValue = `${enteredString}${operatorValue}`;
    }
        enteredString = showValue;
    SHOW_ME.innerText = enteredString;
}




BTN_0.addEventListener('click', zero);
BTN_1.addEventListener('click', one);
BTN_2.addEventListener('click', two);
BTN_3.addEventListener('click', three);
BTN_4.addEventListener('click', four);
BTN_5.addEventListener('click', five);
BTN_6.addEventListener('click', six);
BTN_7.addEventListener('click', seven);
BTN_8.addEventListener('click', eight);
BTN_9.addEventListener('click', nine);
BTN_POS.addEventListener('click', pos);
BTN_DOT.addEventListener('click', dot);

DELETE_BTN.addEventListener('click', deleteHandler);
// CLEAR_BTN.addEventListener('click', clearHandler);

ADD_BTN.addEventListener('click', additionHandler);
MINUS_BTN.addEventListener('click', subtractionHandler);
MUL_BTN.addEventListener('click', multiplicationHandler);
DIV_BTN.addEventListener('click', divisionHandler);
// EQUALS_BTN.addEventListener('click', stringToNumber);

