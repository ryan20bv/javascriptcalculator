

let firstEntry = "";
let secondary = "";
let ternary = "";
let operatorSign = "";
let intMain;
const RESULT = ternary;




function zero(){
    let enteredNumber = BTN_0.value;
    setToMain(enteredNumber);
}
function one(){
    let enteredNumber = BTN_1.value;
    setToMain(enteredNumber);
}
function two(){
    let enteredNumber = BTN_2.value;
    setToMain(enteredNumber);
}
function three(){
    let enteredNumber = BTN_3.value;
    setToMain(enteredNumber);
}
function four(){
    let enteredNumber = BTN_4.value;
    setToMain(enteredNumber);
}
function five(){
    let enteredNumber = BTN_5.value;
    setToMain(enteredNumber);
}
function six(){
    let enteredNumber = BTN_6.value;
    setToMain(enteredNumber);
}
function seven(){
    let enteredNumber = BTN_7.value;
    setToMain(enteredNumber);
}
function eight(){
    let enteredNumber = BTN_8.value;
    setToMain(enteredNumber);
}
function nine(){
    let enteredNumber = BTN_9.value;
    setToMain(enteredNumber);
}
function pos(){
    let enteredNumber = BTN_POS.value;
    setToMain(enteredNumber);
}
function dot(){
    let enteredNumber = BTN_DOT.value;
    setToMain(enteredNumber);
}

function enteredNumberHandler(number){
    console.log(operatorSign);
    
        intMain = parseFloat(number)
    
    console.log(intMain);
}

function setToMain(num){
    firstEntry += num;
    if (num === "0"){
        if (secondary !== "" || ternary !== ""){
            firstEntry = "0";
        } else if( firstEntry === "."){
            firstEntry = "0.";
        } else if ( firstEntry === "0"){
            firstEntry = "";
        }
    }
    enteredNumberHandler(firstEntry);
    displayHandler(firstEntry);
}

function displayHandler (number){
    if(firstEntry === ""){
        DISPLAY.innerHTML = "0";
    }
    else {
        DISPLAY.innerHTML = number;
    }    
}

function remove(){
    let newEntry = firstEntry.slice(0, -1);
    firstEntry = newEntry;
    if (firstEntry === ""){
        setDisplayToZero();
    }
    else {
        displayHandler(firstEntry);
    }
    
}

function setDisplayToZero(){
    firstEntry = "";
    displayHandler(firstEntry);
}
function setShowMeValueToZero(){
    secondary = "";
    ternary = ""
    SHOW_ME.innerText = secondary;
}

function clear(){
    intMain = "";
    operatorSign = "";
    setDisplayToZero();
    setShowMeValueToZero();
    displayHandler(firstEntry);
}

function operation (operator){
    if (firstEntry === ""){
        if (operator === operatorSign){
            operator = "";
        } else {
            operatorSign = operator;
        }
    }
    secondary = `${firstEntry} ${operator}`;   
    firstEntry = "";
    setToShowMe();
}

function addition(){
    const operator = ADD_BTN.value;
    operatorSign = operator;
    operation(operator);
}


function setToShowMe (){
    let showMeValue
    if (ternary === ""){
        ternary = secondary;
        showMeValue = `${secondary} ${firstEntry}`;
    } else {   
        showMeValue = `${ternary} ${secondary}`;
        ternary = showMeValue;
    }
    
    secondary = "";
    SHOW_ME.innerText = showMeValue;
    setDisplayToZero();

}

BTN_0.addEventListener('click', zero);
BTN_1.addEventListener('click', one);
BTN_2.addEventListener('click', two);
BTN_3.addEventListener('click', three);
BTN_4.addEventListener('click', four);
BTN_5.addEventListener('click', five);
BTN_6.addEventListener('click', six);
BTN_7.addEventListener('click', seven);
BTN_8.addEventListener('click', eight);
BTN_9.addEventListener('click', nine);
BTN_POS.addEventListener('click', pos);
BTN_DOT.addEventListener('click', dot);

DELETE_BTN.addEventListener('click', remove);
CLEAR_BTN.addEventListener('click', clear);

ADD_BTN.addEventListener('click', addition);
// MINUS_BTN.addEventListener('click', subtraction);
// MUL_BTN.addEventListener('click', multiplication);
// DIV_BTN.addEventListener('click', division);

