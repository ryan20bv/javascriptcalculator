

let enteredValue = "";
let evOperator = "";
let firstValue = "";
let secondValue = "";
let newOperator = ""
let isOperatorSet = false;
let isFirstNumSet = false;
let total = 0;



const valueHandler = (value) => {
    enteredValue += value;
    console.log(enteredValue);
    if (!isFirstNumSet){
        firstValue = parseInt(enteredValue);
        valueOnShowMe(firstValue);
    } else if (isFirstNumSet){
        secondValue = parseInt(enteredValue);
        valueOnDisplay(secondValue);
    }
}

const valueOnDisplay = (strValue) => {
    return DISPLAY.textContent = strValue;
}

const valueOnShowMe = (strValue) => {
    return SHOW_ME.textContent = strValue;
}

const valueOnTotal = (num) => {
    return TOTAL_DISPLAY.textContent = num
}


const checkOperatorHandler = (operator) => {
    if(!isFirstNumSet){
        newOperator = operator;
        evOperator = firstValue + operator
        valueOnShowMe(evOperator);
        enteredValue = ""
        valueOnDisplay(enteredValue);
        isFirstNumSet = true;
    }
}

const calculate = () => {
    console.log(newOperator)
    if (newOperator === "+"){
        total = firstValue + secondValue;
    } else if (newOperator === "-"){
        total = firstValue - secondValue;
    } else if (newOperator === "*"){
        total = firstValue * secondValue;
    } else if (newOperator === "/"){
        total = firstValue / secondValue
    }
    console.log(firstValue, secondValue);
    console.log(total)
    valueOnTotal(total)
}

const clearHandler = () => {
    firstValue = "";
    secondValue = "";
    total = "";
    isFirstNumSet = false;
    enteredValue = "";
    valueOnShowMe(firstValue);
    valueOnDisplay(secondValue);
    valueOnTotal(total);
}
const sliceHandler= () => {
    enteredValue = enteredValue.slice(0, -1);
    valueOnDisplay(enteredValue);
}

BTN_0.addEventListener('click', valueHandler.bind(this, BTN_0.value));
BTN_1.addEventListener('click', valueHandler.bind(this, BTN_1.value));
BTN_2.addEventListener('click', valueHandler.bind(this, BTN_2.value));
BTN_3.addEventListener('click', valueHandler.bind(this, BTN_3.value));
BTN_4.addEventListener('click', valueHandler.bind(this, BTN_4.value));
BTN_5.addEventListener('click', valueHandler.bind(this, BTN_5.value));
BTN_6.addEventListener('click', valueHandler.bind(this, BTN_6.value));
BTN_7.addEventListener('click', valueHandler.bind(this, BTN_7.value));
BTN_8.addEventListener('click', valueHandler.bind(this, BTN_8.value));
BTN_9.addEventListener('click', valueHandler.bind(this, BTN_9.value));
BTN_POS.addEventListener('click', valueHandler.bind(this, BTN_POS.value));
BTN_DOT.addEventListener('click', valueHandler.bind(this, BTN_DOT.value));

DELETE_BTN.addEventListener('click', sliceHandler);
CLEAR_BTN.addEventListener('click', clearHandler);

ADD_BTN.addEventListener('click', checkOperatorHandler.bind(this, ADD_BTN.value))
MINUS_BTN.addEventListener('click', checkOperatorHandler.bind(this, MINUS_BTN.value))
MUL_BTN.addEventListener('click', checkOperatorHandler.bind(this, MUL_BTN.value))
DIV_BTN.addEventListener('click', checkOperatorHandler.bind(this, DIV_BTN.value))
EQUALS_BTN.addEventListener('click', calculate);